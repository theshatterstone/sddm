# Sddm Theme

## For QT5

Install sugar-dark theme

## For QT6

Install sddm-astronaut-theme

Both themes are in ./usr/share/sddm/themes

Ensure you edit ./etc/sddm.conf.d/sddm.conf so you have the correct
theme selected.
